# This project tests the use of the libpq.a lib in Windows under a ssl connection.

## How to build it and run the tests:

1. Setup the build server
We recommend a Fedora box as it contains a quite complete mingw32 toolset.
You just need the most basic installation, so download the net installation
iso (http://download.fedoraproject.org/pub/fedora/linux/releases/20/Fedora/i386/iso/Fedora-20-i386-netinst.iso),
and, during the installation, choose the minimal setup.

After the OS is installed, you'll have to install some packages:

`yum install mingw32-gcc`
`yum install mingw32-gcc-c++`
`yum install mingw32-make`
`yum install wget`
`yum install tar`
`yum install bzip2`
`yum install mingw32-libzip`
`yum install git`
`yum install cmake`
`yum install mingw32-openssl-static`

2. Build libpq.a

Get the postgresql source code and configure it like this:
`./configure --host=i686-w64-mingw32 --prefix=/usr/i686-w64-mingw32/local/ -with-openssl=yes`

`make && make install`

3. Build GMock and GTest

Get the source code here: https://googlemock.googlecode.com/files/gmock-1.7.0.zip
Unzip it and enter into it.

Create a build dir and enter into it.
build the gmock libs with cmake using the following command:
`cmake -DCMAKE_TOOLCHAIN_FILE=~/postgrestests/mingw32-toolchain.cmake  ..`
Here we suppose that the postgrestests's code is at your home dir.

`make`

Now build the GTest libs:
`cd gmock/gtest && mkdir build && cd build`

Build the GTest libs the same way you built GMock libs:

`cmake -DCMAKE_TOOLCHAIN_FILE=~/postgrestests/mingw32-toolchain.cmake  ..`
`make`

Now edit your ~/.bashrc file to add the entry `export GMOCK_HOME=~/gmock`
So that the variable GMOCK_HOME points to the GMock's root dir.

4. Build this project using cmake:
`cd postgrestests && mkdir build && cd build`
`cmake -DCMAKE_TOOLCHAIN_FILE=../mingw32-toolchain.cmake -DCMAKE_INSTALL_PREFIX=~ ..`




