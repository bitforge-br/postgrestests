#include <gmock/gmock.h>
#include <libpq-fe.h>
#include <iostream>

using std::string;
using std::cerr;
using std::endl;

using namespace ::testing;

class PostgreSQLTests : public Test
{
    public:
        PGconn* m_con;

        virtual void SetUp()
        {
            // By default we will try to connect to a "test"
            // data base using test/test credentials
            string connectionString = "postgresql://test:test@localhost/test?sslmode=require";

            m_con = PQconnectdb(connectionString.c_str());

            if(PQstatus(m_con) != CONNECTION_OK)
            {
                cerr << "Could not connect to the local database \"test\" using test/test credentials" << endl;
                PQfinish(m_con);
            }
        }
};

TEST_F(PostgreSQLTests, TestTableCreation)
{
    PGresult* res = PQexec(m_con, "CREATE TABLE test (id serial, name text);");

    ASSERT_NE(res, nullptr);
    ASSERT_EQ(PQresultStatus(res), PGRES_COMMAND_OK);

    res = PQexec(m_con, "DROP TABLE test;");

}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
